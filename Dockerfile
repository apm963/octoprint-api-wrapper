FROM node:alpine

COPY ["package.json", "yarn.lock", "tsconfig.json", "/usr/src/app/"]
COPY "src" "/usr/src/app/src"
RUN (yarn --version || npm install -g yarn) && cd /usr/src/app/ && mkdir dist && yarn && yarn run build

WORKDIR /usr/src/app
EXPOSE 3000
CMD ["yarn", "start"]