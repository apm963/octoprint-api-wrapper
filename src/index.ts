import * as express from 'express';
import * as got from 'got';
import {argv} from 'yargs';

type StopReason = NodeJS.Signals | 'exit' | 'uncaughtException';

type CacheItem<T> = null | CacheItemIface<T>;

interface CacheItemIface<T> {
	time: number;
	data: T;
}

interface OctoprintApiJob {
	job: {
		averagePrintTime: unknown, // null
		estimatedPrintTime: unknown, // null
		filament: unknown, // null
		file: {
			"date": number, // 1559393315
			"display": string, // "Watch_Stand_Part1_remix_v2 - Fossil_185.gcode"
			"name": string, // "Watch_Stand_Part1_remix_v2_-_Fossil_185.gcode"
			"origin": string, // "local"
			"path": string, // "Watch_Stand_Part1_remix_v2_-_Fossil_185.gcode"
			"size": number // 3596534
		},
		lastPrintTime: unknown, // null
		user: string, // "myUser"
	},
	progress: {
		completion: number, // float: 73.34302970582233
		filepos: number, // 2637807
		printTime: number, // 6944
		printTimeLeft: number, // 2516
		printTimeLeftOrigin: string // "estimate"
	},
	state: string, // "Printing"
}

const webserverPort: number = parseInt( (argv.webserverPort as undefined|string) || process.env.PORT || '3000' );
const octoprintProtocol: string = (argv.octoprintProtocol as undefined|string) || process.env.OCTOPRINT_PROTOCOL || 'http';
const octoprintHost: string = (argv.octoprintHost as undefined|string) || process.env.OCTOPRINT_HOST || '';
const octoprintPort: number = parseInt((argv.octoprintPort as undefined|string) || process.env.OCTOPRINT_PORT || '5000');
const octoprintPathPrefix: string = (argv.octoprintPathPrefix as undefined|string) || process.env.OCTOPRINT_PATH_PREFIX || ''; // if behind reverse proxy
const octoprintApiKey: string = (argv.octoprintApiKey as undefined|string) || process.env.OCTOPRINT_API_KEY || '';
const remoteApiCacheDuration: number = parseInt((argv.cacheDuration as undefined|string) || process.env.CACHE_DURATION || '5'); // Prevent hammering the remote server

const app = express();
const cache: { job: CacheItem<OctoprintApiJob> } = { job: null };

function buildOctoprintApiUrl(apiPath: string): string {
	const path = `/${octoprintPathPrefix}/${apiPath}`;
	return `${octoprintProtocol}://${octoprintHost}:${octoprintPort}${ path.replace(/\/{2,}/g, '/') }`;
}

async function getJobStatus(): Promise<OctoprintApiJob | null> {
	
	// Attempt to serve cached data
	if (remoteApiCacheDuration > 0 && cache.job && (cache.job.time + (remoteApiCacheDuration * 1000)) > Date.now()) {
		return cache.job.data;
	}
	
	try {
		const response = await got(buildOctoprintApiUrl('/api/job'), { headers: { 'X-Api-Key': octoprintApiKey } });
		const parsedBody: unknown|OctoprintApiJob = JSON.parse(response.body);
		if (typeof parsedBody === 'object' && parsedBody && 'job' in parsedBody && 'progress' in parsedBody && 'state' in parsedBody) {
			const job = parsedBody as OctoprintApiJob;
			// Cache
			if (remoteApiCacheDuration > 0) {
				cache.job = {
					time: Date.now(),
					data: job,
				};
			}
			return job;
		}
		return null;
	}
	catch (err) {
		console.error( (err && err.response && err.response.body) || err );
		return null;
	}
}

const router = express.Router();

router.use(async (_req, res, next) => {
	try {
		const job: null | OctoprintApiJob = await getJobStatus();
		if (!job) {
			return res.status(500).send(job);
		}
		res.locals.job = job;
		return next();
	}
	catch (err) {
		return res.status(500).send(err);
	}
});

router.get('/', (_req, res) => res.send((res.locals.job as OctoprintApiJob)));
router.get('/file', (_req, res) => res.send((res.locals.job as OctoprintApiJob).job.file));
router.get('/initiated-by', (_req, res) => res.send((res.locals.job as OctoprintApiJob).job.user));
router.get('/state', (_req, res) => res.send((res.locals.job as OctoprintApiJob).state));
router.get('/progress', (_req, res) => res.send((res.locals.job as OctoprintApiJob).progress));
router.get('/progress/total-print-time', (_req, res) => {
	const { progress } = (res.locals.job as OctoprintApiJob);
	return res.send( (progress.printTime + progress.printTimeLeft).toString() );
});
router.get('/progress/time-remaining-percentage', (_req, res) => {
	const { progress } = (res.locals.job as OctoprintApiJob);
	const totalPrintTime = (progress.printTime + progress.printTimeLeft);
	return res.send( ((totalPrintTime - progress.printTimeLeft) / totalPrintTime).toString() );
});

app.use('/api/job', router);

console.log('Started with args:');
console.dir({ webserverPort, octoprintProtocol, octoprintHost, octoprintPort, octoprintPathPrefix, remoteApiCacheDuration });

const server = app.listen(webserverPort, () => console.log(`App listening on port ${webserverPort}!`));

let serverRunning = true;
const stopServer = (signal: StopReason) => {
	console.log(`Received [${signal}]; server is ${serverRunning ? 'going' : 'already'} down`);
	server.close();
	serverRunning = false;
};

process.on('exit', () => stopServer('exit'));
process.on('SIGINT', () => stopServer('SIGINT'));
process.on('SIGTERM', () => stopServer('SIGTERM'));
process.on('uncaughtException', () => stopServer('uncaughtException'));
