# Octoprint API wrapper

This allows "safely" exposing a subset of readonly Octoprint API functionality to the internet. It also supports a data caching layer for efficient API calls.

## Development

```sh
yarn # fetch dependencies
yarn start-dev --octoprint-host YOURHOST --octoprint-api-key 'YOURKEY'
```

## Production

Edit the environment vars in the `docker-compose.yml` to your setup and:

```sh
docker build -t octoprint-api-wrapper .

# For swarm deployment
docker stack deploy -c docker-compose.yml octoprint-api-wrapper

# Or for docker run (replace env vars)
docker run -d --name octoprint-api-wrapper -p 4000:3000 -e 'OCTOPRINT_HOST=yourhost' -e 'OCTOPRINT_API_KEY=yourkey' --restart always octoprint-api-wrapper:latest
```
